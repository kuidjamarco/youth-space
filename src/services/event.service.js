const { executeQuery } = require('../config/db-sync')
const uuid = require('uuid')

exports.findAll = () => {
  return new Promise((resolve, reject) => {
    executeQuery(
      `
      SELECT 
        event_id, name, theme, event_flyer, starting_date, ending_date, closing_date, registration_fee, location 
      FROM 
        event
      ORDER BY starting_date ASC
      `,
      (error, result) => {
        if (error) reject(`Get all events failed: ${error.message}`)
        resolve(result && result[0]?.event_id ? result : [])
      }
    )
  })
}

exports.findOne = (event_id) => {
  return new Promise((resolve, reject) => {
    executeQuery(
      `
      SELECT * FROM event WHERE
        event_id = '${event_id}'
      LIMIT 1
    `,
      (error, result) => {
        if (error) reject(`Get event details failed: ${error.message}`)
        else resolve(result[0])
      }
    )
  })
}

exports.create = (data, created_by) => {
  const {
    name,
    theme,
    starting_date,
    ending_date,
    closing_date,
    registration_fee,
    location,
    event_flyer,
  } = data
  return new Promise((resolve, reject) => {
    if (
      !name ||
      !theme ||
      !starting_date ||
      !ending_date ||
      !closing_date ||
      !location ||
      !created_by
    )
      reject('Incomplete Data Supply')
    executeQuery(
      `
      INSERT INTO 
        event (
          event_id,
          name,
          theme,
          starting_date,
          ending_date,
          closing_date,
          registration_fee,
          location,
          event_flyer,
          created_by
        ) 
      VALUES (
        "${uuid.v4()}", 
        "${name}", 
        "${theme}", 
        "${starting_date}", 
        "${ending_date}", 
        "${closing_date}", 
        "${registration_fee}", 
        "${location}", 
        "${event_flyer}", 
        "${created_by}"
      );
    `,
      (error, result) => {
        if (error) reject(`Create new event Failed: ${error.message}`)
        else resolve(result)
      }
    )
  })
}

exports.update = (event) => {
  return new Promise((resolve, reject) => {
    const { event_id, action, ...data } = event
    let update_string = Object.keys(data).reduce(
      (p_value, key) => p_value + key + ' = "' + data[key] + '", ',
      ''
    )
    executeQuery(
      `UPDATE event SET ${update_string.substring(
        0,
        update_string.lastIndexOf(', ')
      )} WHERE event_id = '${event_id}'`,
      (error, result) => {
        if (error) reject(`Login update failed: ${error.message}`)
        else resolve(result)
      }
    )
  })
}

exports.delete = (event_id) => {
  return new Promise((resolve, reject) => {
    executeQuery(
      `
      DELETE FROM event WHERE event_id = "${event_id}"
    `,
      (error, result) => {
        if (error) reject(`Create new event Failed: ${error.message}`)
        else resolve(result)
      }
    )
  })
}

exports.findFeatureEvents = async () => {
  const feature_events = await new Promise((resolve, reject) => {
    executeQuery(
      `
      SELECT 
        event.event_id as event_id, name, theme, starting_date, COUNT(event.event_id) as number_of_guest
      FROM 
        event 
      LEFT JOIN 
        registration 
      ON 
        event.event_id = registration.event_id
      WHERE
        DAYOFYEAR(ending_date) > DAYOFYEAR(CURRENT_DATE())
      GROUP BY 
        event.event_id
      ORDER BY 
        starting_date
      ASC LIMIT 5`,
      (error, result) => {
        if (error) reject(`Get feature events failed: ${error.message}`)
        resolve(result)
      }
    )
  })
  const number_of_events = await new Promise((resolve, reject) => {
    executeQuery(
      `
        SELECT count(event_id) AS  number_of_events FROM event`,
      (error, result) => {
        if (error) reject(`Count events failed: ${error.message}`)
        resolve(result)
      }
    )
  })

  return {
    feature_events,
    number_of_events: number_of_events[0]?.number_of_events,
  }
}
