const { executeQuery } = require('../config/db-sync')
const uuid = require('uuid')

exports.create = ({ visitor_ip, email }) => {
  if (visitor_ip) {
    return new Promise((resolve, reject) => {
      executeQuery(
        `INSERT INTO visitor(visitor_id, visitor_ip, email) VALUES("${uuid.v4()}", "${visitor_ip}", "${email}")`,
        (error, result) => {
          if (error) reject(`Could not add visitor: ${error.message}`)
          else resolve(result)
        }
      )
    })
  }
  return
}

exports.getStats = async () => {
  const number_of_visitors = await new Promise((resolve, reject) => {
    executeQuery(
      `SELECT COUNT(*) AS number_of_visitors FROM visitor`,
      (error, result) => {
        if (error) reject(`Could not count visitors: ${error.message}`)
        else resolve(result)
      }
    )
  })
  const weekly_visitors = await new Promise((resolve, reject) => {
    executeQuery(
      `
      SELECT
       COUNT(*) AS weekly_visitors
      FROM 
        visitor 
      WHERE 
        visited_at > DATE_SUB(CURRENT_DATE(), INTERVAL 1 WEEK)`,
      (error, result) => {
        if (error) reject(`Could not count visitors: ${error.message}`)
        else resolve(result)
      }
    )
  })
  const monthly_visitors = await new Promise((resolve, reject) => {
    executeQuery(
      `
      SELECT
       COUNT(*) AS monthly_visitors
      FROM 
        visitor 
      WHERE 
        visited_at > DATE_SUB(CURRENT_DATE(), INTERVAL 1 MONTH)`,
      (error, result) => {
        if (error) reject(`Could not count visitors: ${error.message}`)
        else resolve(result)
      }
    )
  })
  const today_visitors = await new Promise((resolve, reject) => {
    executeQuery(
      `SELECT COUNT(*) AS today_visitors FROM visitor WHERE  DAYOFYEAR(visited_at) = DAYOFYEAR(CURRENT_DATE())`,
      (error, result) => {
        if (error) reject(`Could not count visitors: ${error.message}`)
        else resolve(result)
      }
    )
  })
  return {
    ...today_visitors[0],
    ...weekly_visitors[0],
    ...monthly_visitors[0],
    ...number_of_visitors[0],
  }
}

exports.getVisitEvolution = () => {
  return new Promise((resolve, reject) => {
    executeQuery(
      `SELECT DATE(visited_at) AS date_of_year, COUNT(*) AS value FROM visitor GROUP BY DAYOFYEAR(visited_at)`,
      (error, result) => {
        if (error) reject(`Could not count visitors: ${error.message}`)
        else resolve(result)
      }
    )
  })
}
