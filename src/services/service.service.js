const { executeQuery } = require('../config/db-sync')
const uuid = require('uuid')

exports.create = (service, created_by) => {
  return new Promise((resolve, reject) => {
    const { action, ...data } = service
    executeQuery(
      `INSERT INTO service(service_id, ${Object.keys(
        data
      )}, created_by) VALUES("${uuid.v4()}", "${Object.values(data).join(
        `", "`
      )}", "${created_by}")`,
      (error, result) => {
        if (error) reject(`Failed to add statistics: ${error.message}`)
        else resolve(result)
      }
    )
  })
}

exports.findAll = async () => {
  return new Promise((resolve, reject) => {
    executeQuery(`SELECT * FROM service`, (error, result) => {
      if (error) reject(`Could not count visitors: ${error.message}`)
      else resolve(result)
    })
  })
}
