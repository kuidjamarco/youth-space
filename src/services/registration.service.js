const { executeQuery } = require('../config/db-sync')
const uuid = require('uuid')

exports.create = (registration) => {
  const { user_id, event_id } = registration
  return new Promise((resolve, reject) => {
    if (!user_id || !event_id) reject('Incomplete Data Supply')
    executeQuery(
      `
        INSERT INTO 
            registration(
                registration_id, 
                user_id, 
                event_id
            ) 
        VALUES("${uuid.v4()}", "${user_id}", "${event_id}")`,
      (error, result) => {
        if (error) reject(`User acount creation failed: ${error.message}`)
        else resolve(result)
      }
    )
  })
}

exports.findAll = (event_id) => {
  return new Promise((resolve, reject) => {
    executeQuery(
      `
        SELECT 
          user.user_id as user_id, theme, first_name, last_name, image_ref, profession, assembly.name as assembly, legal_status, telephone, pastor_telephone, leader_telephone, leader_name, pastor_name
        FROM
          registration
        INNER JOIN
            event
        ON
            registration.event_id = event.event_id 
        INNER JOIN (
                user 
            INNER JOIN
                assembly
            ON
                user.assembly_id = assembly.assembly_id 
        )
        ON 
            registration.user_id = user.user_id
        WHERE 
            event.event_id = "${event_id}"
        `,
      (error, result) => {
        if (error) reject(`Get all events failed: ${error.message}`)
        resolve(result)
      }
    )
  })
}

exports.findUserAll = (user_id) => {
  return new Promise((resolve, reject) => {
    executeQuery(
      `
        SELECT 
          registration_id, registered_at, event.theme
        FROM
          registration
        INNER JOIN
          event
        ON
          registration.event_id = event.event_id 
        WHERE 
            registration.user_id = "${user_id}"
        `,
      (error, result) => {
        if (error) reject(`Get all events failed: ${error.message}`)
        resolve(result)
      }
    )
  })
}

exports.hasRegistered = (user_id, event_id) => {
  return new Promise((resolve, reject) => {
    executeQuery(
      `
        SELECT  * FROM registration
        WHERE  user_id = "${user_id}"
        AND event_id = "${event_id}" 
        `,
      (error, result) => {
        if (error) reject(`Get all events failed: ${error.message}`)
        resolve(result[0] ? true : false)
      }
    )
  })
}
