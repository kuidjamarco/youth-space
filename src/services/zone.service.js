const { executeQuery } = require('../config/db-sync')

exports.findAll = () => {
  return new Promise((resolve, reject) => {
    executeQuery(`SELECT * FROM zone`, (error, result) => {
      if (error) reject(`Falied to fetch zones: ${error.message}`)
      else resolve(result)
    })
  })
}
