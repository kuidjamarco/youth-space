const { createTransporter } = require('../config/nodemailer')
const { executeQuery } = require('../config/db-sync')
const bcrypt = require('bcrypt')
const uuid = require('uuid')

exports.create = (user) => {
  return new Promise((resolve, reject) => {
    const user_id = uuid.v4()
    executeQuery(
      `INSERT INTO user(user_id, ${Object.keys(
        user
      )}) VALUES("${user_id}", "${Object.values(user).join(`", "`)}")`,
      (error, result) => {
        if (error) reject(`User acount creation failed: ${error.message}`)
        else resolve({ ...result, user_id })
      }
    )
  })
}

exports.updateLogin = (user_id, telephone, password) => {
  return new Promise((resolve, reject) => {
    const crypted_password = bcrypt.hashSync(password, 10)
    executeQuery(
      `UPDATE user SET telephone = '${telephone}', password = '${crypted_password}' WHERE user_id = '${user_id}'`,
      (error, result) => {
        if (error) reject(`Login update failed: ${error.message}`)
        else resolve(result)
      }
    )
  })
}

exports.updateUser = (user) => {
  return new Promise((resolve, reject) => {
    const { user_id, created_at, password, ...data } = user
    let update_string = Object.keys(data).reduce(
      (p_value, key) => p_value + key + ' = "' + user[key] + '", ',
      ''
    )
    executeQuery(
      `UPDATE user SET ${update_string.substring(
        0,
        update_string.lastIndexOf(', ')
      )} WHERE user_id = '${user_id}'`,
      (error, result) => {
        if (error) reject(`User update failed: ${error.message}`)
        else resolve(result)
      }
    )
  })
}

exports.findAll = (assembly_id) => {
  return new Promise((resolve, reject) => {
    executeQuery(
      `SELECT 
        user.user_id as user_id, first_name, last_name, image_ref, assembly.name as assembly 
      FROM
        user
      INNER JOIN
        assembly
      ON 
        user.assembly_id = assembly.assembly_id
      WHERE 
        assembly.assembly_id = "${assembly_id}"`,
      (error, result) => {
        if (error) reject(`User login failed: ${error.message}`)
        else resolve(result)
      }
    )
  })
}

exports.findOne = (user_id) => {
  return new Promise((resolve, reject) => {
    executeQuery(
      `SELECT 
      user.user_id as user_id, email, gender, first_name, last_name, image_ref, profession, assembly.name as assembly, legal_status, telephone 
      FROM
        user
      INNER JOIN
        assembly
      ON 
        user.assembly_id = assembly.assembly_id
      WHERE 
        user.user_id = "${user_id}"`,
      (error, result) => {
        if (error) reject(`Failed to find one: ${error.message}`)
        else resolve(result[0])
      }
    )
  })
}

exports.login = (telephone, password) => {
  return new Promise((resolve, reject) => {
    executeQuery(
      `SELECT * FROM user WHERE telephone = "${telephone}"`,
      (error, result) => {
        if (error) reject(`User login failed: ${error.message}`)
        let user_index = -1
        for (let i = 0; i < result.length; i++) {
          const { password: pwd } = result[i]
          if (bcrypt.compareSync(password, pwd)) user_index = i
        }
        if (user_index === -1) reject('User telephone or password not correct')
        else resolve(result[user_index])
      }
    )
  })
}

exports.hasAcount = (telephone) => {
  return new Promise((resolve, reject) => {
    executeQuery(
      `SELECT * FROM user WHERE telephone = "${telephone}"`,
      (error, result) => {
        if (error) reject(`User login failed: ${error.message}`)
        else resolve(result[0] ? true : false)
      }
    )
  })
}

exports.sendMails = async (newEvent) => {
  const { event_id, name, starting_date, theme, location } = newEvent
  const transporter = await createTransporter()
  return new Promise((resolve, reject) => {
    executeQuery(`SELECT email FROM user`, async (error, emails) => {
      if (error) reject(`User login failed: ${error.message}`)
      // send mail with defined transport object
      let info = await transporter.sendMail({
        from: '"Fred Foo 👻" <foo@example.com>', // sender address
        to: emails.join(', '), // list of receivers
        subject: `New event on <a href='#'>Youth space</a>`, // Subject line
        html: `<div style="display: grid; justify-content: center;  grid-auto-flow: column;">
          <div class="about" style="
              width: 50vw; text-align: center; font-family: 'Times New Roman', Times, serif;">
              <h3>${name}</h3>
              <p>
              <p><b>Starting Date:</b> ${starting_date}</p>
              <p><b>Theme: </b>${theme} XAF</p>
              <p><b>Event Hosting place: </b>${location}</p>
            </p>
            <p>
                <a href="https://alertevents.online/event/${event_id}">Consulter</a>
            </p>
        </div>
        </div>`, // html body
      })
      resolve(info)
    })
  })
}

exports.getStats = async () => {
  const number_of_registrations = await new Promise((resolve, reject) => {
    executeQuery(
      `SELECT COUNT(*) AS number_of_registrations FROM registration`,
      (error, result) => {
        if (error) reject(`Could not count registrations: ${error.message}`)
        else resolve(result)
      }
    )
  })
  const weekly_registrations = await new Promise((resolve, reject) => {
    executeQuery(
      `
      SELECT
       COUNT(*) AS weekly_registrations
      FROM 
        registration 
      WHERE 
        registered_at >DATE_SUB(CURRENT_DATE(), INTERVAL 1 WEEK)`,
      (error, result) => {
        if (error) reject(`Could not count registrations: ${error.message}`)
        else resolve(result)
      }
    )
  })
  const monthly_registrations = await new Promise((resolve, reject) => {
    executeQuery(
      `
      SELECT
       COUNT(*) AS monthly_registrations
      FROM 
        registration 
      WHERE 
        registered_at > DATE_SUB(CURRENT_DATE(), INTERVAL 1 MONTH)`,
      (error, result) => {
        if (error) reject(`Could not count registrations: ${error.message}`)
        else resolve(result)
      }
    )
  })
  const today_registrations = await new Promise((resolve, reject) => {
    executeQuery(
      `SELECT COUNT(*) AS today_registrations FROM registration WHERE  DAYOFYEAR(registered_at) = DAYOFYEAR(CURRENT_DATE())`,
      (error, result) => {
        if (error) reject(`Could not count registrations: ${error.message}`)
        else resolve(result)
      }
    )
  })
  return {
    ...today_registrations[0],
    ...weekly_registrations[0],
    ...monthly_registrations[0],
    ...number_of_registrations[0],
  }
}

exports.getRegistrationEvolution = () => {
  return new Promise((resolve, reject) => {
    executeQuery(
      `SELECT DATE(registered_at) AS date_of_year, COUNT(*) AS value FROM registration GROUP BY DAYOFYEAR(registered_at)`,
      (error, result) => {
        if (error) reject(`Could not count registration: ${error.message}`)
        else resolve(result)
      }
    )
  })
}
