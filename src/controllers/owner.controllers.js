const { findFeatureEvents } = require('../services/event.service')
const {
  getStats: getRegistrationStats,
  getRegistrationEvolution,
} = require('../services/user.service')
const {
  getStats: getVistorStats,
  getVisitEvolution,
} = require('../services/visitor.service')

exports.getOwnerBoard = async (req, res) => {
  const user = req.session.user
  try {
    res.render('owner', {
      user: {
        ...user,
        is_owner: user?.user_type === 'OWNER',
        is_admin: user?.user_type === 'MANAGER',
      },
      ...(await getVistorStats()),
      ...(await findFeatureEvents()),
      ...(await getRegistrationStats()),
    })
  } catch (error) {
    res.render('errors/500', {
      user: {
        ...user,
        is_owner: user?.user_type === 'OWNER',
        is_admin: user?.user_type === 'MANAGER',
      },
      referer_link: req.headers.referer?.split('?')[0],
      message: error,
    })
  }
}

exports.getWebStats = async (req, res) => {
  try {
    return res.json({
      visitors: await getVisitEvolution(),
      registrations: await getRegistrationEvolution(),
    })
  } catch (error) {
    const urlPath = req.headers.referer?.split('?')[0]
    res.redirect(`${urlPath}?error=${error}`)
  }
}
