const {
  create: addNewEvent,
  delete: deleteEvent,
  findOne: findEventDetail,
  update: updateEvent,
  findAll: findAllEvents,
  findOne,
} = require('../services/event.service')
const { sendMails } = require('../services/user.service')
const { findAll: findAllAssemblies } = require('../services/assembly.service')
const {
  findAll: findAllParticipants,
} = require('../services/registration.service')
const { findAll: findAllZones } = require('../services/zone.service')
const { catchFile } = require('../config/formidable')

//@desc add new event
exports.createEvent = async (req, res) => {
  const user = req.session.user
  try {
    const newEvent = await catchFile(req)
    await addNewEvent(newEvent, user.user_id)
    // await sendMails(newEvent)
    res.render('admin', {
      success: `event has been successfully ${
        newEvent.event_id === '' ? 'created' : 'modified'
      }`,
      ...newEvent,
      user: {
        ...user,
        is_owner: user?.user_type === 'OWNER',
        is_admin: user?.user_type === 'MANAGER',
      },
    })
  } catch (error) {
    res.render('errors/500', {
      user: {
        ...user,
        is_owner: user?.user_type === 'OWNER',
        is_admin: user?.user_type === 'MANAGER',
      },
      referer_link: req.headers.referer?.split('?')[0],
      message: error,
    })
  }
}

exports.updateEvent = async (req, res) => {
  const urlPath = req.headers.referer?.split('?')[0]
  try {
    await updateEvent(req.body)
    res.redirect(`${urlPath}`)
  } catch (error) {
    res.redirect(`${urlPath}?error=${error}`)
  }
}

exports.updateEventImage = async (req, res) => {
  const urlPath = req.headers.referer?.split('?')[0]
  try {
    const newEvent = await catchFile(req)
    const event = await findOne(newEvent.event_id)
    await updateEvent(newEvent)
    try {
      if (event?.event_flyer)
        fs.unlinkSync(`../../public/images/${event?.event_flyer}`)
    } catch (error) {
      console.log(error)
    }
    res.redirect(`${urlPath}`)
  } catch (error) {
    res.redirect(`${urlPath}?error=${error}`)
  }
}

//@desc fetch all events informations
exports.getAllEvents = async (req, res) => {
  const events = await findAllEvents()
  res.json({ events })
}

//@desc fetch all events informations
exports.getOneEvent = async (req, res) => {
  const event = await findOne(req.params.event_id)
  res.json({ event })
}

//@desc fetch all event participants informations
exports.getAllParticipants = async (req, res) => {
  const participants = await findAllParticipants(req.params.event_id)
  res.json({ participants })
}

//@desc fetch selected event detail for registrations
exports.renderRegistrationForm = async (req, res) => {
  try {
    const event_id = req.params.event_id
    const user = req.session.user

    const assemblies = await findAllAssemblies()
    const eventDetail = await findEventDetail(event_id)
    const events = await findAllEvents()
    const zones = await findAllZones()
    res.render('registration', {
      ...eventDetail,
      assemblies,
      event_id,
      zones,
      eventImages: events.map(({ event_flyer }) => ({
        event_flyer,
      })),
      user: {
        ...user,
        is_owner: user?.user_type === 'OWNER',
        is_admin: user?.user_type === 'MANAGER',
      },
    })
  } catch (error) {
    const urlPath = req.headers.referer?.split('?')[0]
    res.redirect(`${urlPath}?error=${error}`)
  }
}

//@desc deleted selected event
exports.deleteEvent = async (req, res) => {
  try {
    await deleteEvent(req.params.event_id)

    const urlPath = req.headers.referer?.split('?')[0]
    res.redirect(urlPath)
  } catch (error) {
    const urlPath = req.headers.referer?.split('?')[0]
    res.redirect(`${urlPath}?error=${error}`)
  }
}
