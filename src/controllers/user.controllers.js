const fs = require('fs')
const {
  login,
  hasAcount,
  updateUser,
  updateLogin,
  create: createUserAccount,
  findOne,
} = require('../services/user.service')
const {
  hasRegistered,
  create: registerUser,
  findUserAll: findUserRegistrations,
} = require('../services/registration.service')
const { catchFile } = require('../config/formidable')
const { create: addVisitor } = require('../services/visitor.service')
const { findAll: findAllEvents } = require('../services/event.service')
const { findAll: findAssemblies } = require('../services/assembly.service')

exports.renderHome = async (req, res) => {
  try {
    const events = await findAllEvents()
    await addVisitor({ visitor_ip: req.socket.remoteAddress })
    const user = req.session.user
    res.render('home', {
      events,
      user: {
        ...user,
        is_owner: user?.user_type === 'OWNER',
        is_admin: user?.user_type === 'MANAGER',
      },
    })
  } catch (error) {
    res.render('errors/500', {
      referer_link: req.headers.referer?.split('?')[0],
      message: error,
    })
  }
}

exports.renderUser = async (req, res) => {
  try {
    const user = req.session.user
    const assemblies = await findAssemblies()
    const events = await findAllEvents()
    const registrations = await findUserRegistrations(user?.user_id)
    res.render('user', {
      registrations,
      events,
      user: {
        assemblies,
        ...(await findOne(user?.user_id)),
        is_owner: user?.user_type === 'OWNER',
        is_admin: user?.user_type === 'MANAGER',
      },
    })
  } catch (error) {
    res.render('errors/500', {
      referer_link: req.headers.referer?.split('?')[0],
      message: error,
    })
  }
}

//desc new user resgistration
exports.registerUser = async (req, res) => {
  try {
    const {
      event_id,
      first_name,
      last_name,
      email,
      gender,
      telephone,
      profession,
      assembly_id,
      image_ref,
      legal_status,
    } = await catchFile(req)
    const user_has_account = await hasAcount(telephone)
    if (user_has_account) {
      const urlPath = req.headers.referer?.split('?')[0]
      res.redirect(`${urlPath}?error=Please login to registered`)
    }
    const user = await createUserAccount({
      first_name,
      last_name,
      email,
      gender,
      telephone,
      profession,
      assembly_id,
      image_ref,
      legal_status,
    })
    await registerUser({ user_id: user.user_id, event_id })

    req.session.user = user
    res.render('reset-password', {
      user,
      telephone,
    })
  } catch (error) {
    res.render('errors/500', {
      referer_link: req.headers.referer?.split('?')[0],
      message: error,
    })
  }
}

//desc old user registration
exports.registerWithAccount = async (req, res) => {
  const urlPath = req.headers.referer?.split('?')[0]
  try {
    const user_id = req.session.user.user_id
    const event_id = req.params.event_id
    const user_has_registered = await hasRegistered(user_id, event_id)
    if (user_has_registered)
      throw new Error('You have already registered for this event')
    await registerUser({
      user_id,
      event_id,
    })
    res.redirect(
      `${urlPath}?success=You have successfully register to your event`
    )
  } catch (error) {
    res.redirect(`${urlPath}?error=${error}`)
  }
}
//desc add new user
exports.addVisitor = async (req, res) => {
  const urlPath = req.headers.referer?.split('?')[0]
  try {
    const { email } = req.body
    await addVisitor({ email, visitor_ip: req.socket.remoteAddress })

    res.redirect(urlPath)
  } catch (error) {
    res.redirect(`${urlPath}?error=${error}`)
  }
}

exports.loginUser = async (req, res) => {
  const { telephone, password } = req.body
  let urlPath = req.headers.referer?.split('?')[0]
  try {
    const AccessibleRoutes = {
      OWNER: '/owner',
      MANAGER: '/admin',
      USER: '/user',
    }
    req.session.user = await login(telephone, password)

    if (req.session.user) {
      urlPath = AccessibleRoutes[req.session.user?.user_type] ?? urlPath
    }
    res.redirect(urlPath)
  } catch (error) {
    res.redirect(`${urlPath}?error=${error}`)
  }
}

exports.updateLogin = async (req, res) => {
  try {
    const user = req.session.user
    const { telephone, password, confirm_password } = req.body

    if (password !== confirm_password)
      throw new Error('Password does not match confirmed pasword')
    if (password === user.password && telephone === user.telephone)
      throw new Error('No change detected')
    else {
      await updateLogin(user.user_id, telephone, password)
      await this.loginUser(req, res)
    }
  } catch (error) {
    const urlPath = req.headers.referer?.split('?')[0]
    res.redirect(`${urlPath}?error=${error}`)
  }
}

exports.updateUser = async (req, res) => {
  const urlPath = req.headers.referer?.split('?')[0]
  const user = req.session.user
  try {
    const { password, action, ...new_user } = req.body
    await updateUser({ ...user, ...new_user })
    req.session.user = { ...user, ...new_user }
    res.redirect(
      `/user?success=Your user account informations have been successfully updated`
    )
  } catch (error) {
    res.redirect(`${urlPath}?error=${error}`)
  }
}

exports.updateImage = async (req, res) => {
  const urlPath = req.headers.referer?.split('?')[0]
  try {
    const user = req.session.user
    const { image_ref } = await catchFile(req)
    await updateUser({ ...user, image_ref })
    req.session.user.image_ref = image_ref
    try {
      if (user?.image_ref)
        fs.unlinkSync(`../../public/images/${user?.image_ref}`)
    } catch (error) {
      console.log(error)
    }
    res.redirect(
      `${urlPath}?success=Your user account informations have been successfully updated`
    )
  } catch (error) {
    res.redirect(`${urlPath}?error=${error}`)
  }
}
