const {
  findAll: findAllAssemblies,
  create: addNewAssembly,
  update: updateAssembly,
  delete: deleteAssembly,
} = require('../services/assembly.service')
const { findAll: findAssemblyMembers } = require('../services/user.service')

exports.renderAssemblies = async (req, res) => {
  try {
    const assemblies = await findAllAssemblies()
    const user = req.session.user
    res.render('assemblies', {
      assemblies,
      user: {
        ...user,
        is_owner: user?.user_type === 'OWNER',
        is_admin: user?.user_type === 'MANAGER',
      },
    })
  } catch (error) {
    res.render('errors/500', {
      referer_link: req.headers.referer?.split('?')[0],
      message: error,
    })
  }
}

exports.renderAssemblyMembers = async (req, res) => {
  try {
    const members = await findAssemblyMembers(req.params.assembly_id)
    const user = req.session.user
    res.render('home', {
      members,
      assembly: members[0]?.assembly,
      user: {
        ...user,
        is_owner: user?.user_type === 'OWNER',
        is_admin: user?.user_type === 'MANAGER',
      },
    })
  } catch (error) {
    res.render('errors/500', {
      referer_link: req.headers.referer?.split('?')[0],
      message: error,
    })
  }
}

exports.editAssembly = async (req, res) => {
  const urlPath = req.headers.referer?.split('?')[0]
  try {
    let newAssembly = req.body
    if (newAssembly.assembly_id !== '') await updateAssembly(newAssembly)
    else await addNewAssembly(newAssembly)

    res.redirect(
      `${urlPath}?success=Your assembly has been successfully added. Get register now`
    )
  } catch (error) {
    res.redirect(`${urlPath}?error=${error}`)
  }
}

//@desc deleted selected event
exports.deleteAssembly = async (req, res) => {
  const urlPath = req.headers.referer?.split('?')[0]
  try {
    await deleteAssembly(req.params.assembly_id)

    res.redirect(urlPath)
  } catch (error) {
    res.redirect(`${urlPath}?error=${error}`)
  }
}
