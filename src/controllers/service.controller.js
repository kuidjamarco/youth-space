const {
  create: createService,
  findAll,
} = require('../services/service.service')

exports.addStatistics = async (req, res) => {
  const user = req.session.user
  const urlPath = req.headers.referer?.split('?')[0]
  try {
    await createService(req.body, user.user_id)
    res.redirect(`${urlPath}?success=Statistics has been saved successfuly`)
  } catch (error) {
    console.error({ error })
    res.redirect(`${urlPath}?error=${error}`)
  }
}

exports.getServiceStats = async (req, res) => {
  try {
    return res.json({
      services: await findAll(),
    })
  } catch (error) {
    const urlPath = req.headers.referer?.split('?')[0]
    res.redirect(`${urlPath}?error=${error}`)
  }
}
