const {
  findAll: findAllEvents,
} = require('../services/event.service')
const {
  findAll: findAllParticipants,
} = require('../services/registration.service')
const { findAll: findAllAssemblies } = require('../services/assembly.service')
const { findAll: findAllZones } = require('../services/zone.service')

// @desc  new event adding form
exports.renderAdminPage = async (req, res) => {
  try {
    const user = req.session.user

    const zones = await findAllZones()
    const events = await findAllEvents()
    const assemblies = await findAllAssemblies()
    res.render('admin', {
      zones,
      assemblies,
      user: {
        ...user,
        is_owner: user?.user_type === 'OWNER',
        is_admin: user?.user_type === 'MANAGER',
      },
      events: events.map((event) => ({
        ...event,
        starting_date: event.starting_date?.toLocaleString(),
        ending_date: event.ending_date?.toLocaleString(),
        closing_date: event.closing_date?.toLocaleString(),
      })),
    })
  } catch (error) {
    console.log(error)
    res.render('errors/500', {
      referer_link: req.headers.referer?.split('?')[0],
      message: error,
    })
  }
}

exports.renderPrinter = async (req, res) => {
  const user = req.session.user
  try {
    const { ids, event_id } = req.query
    if (!ids)
      throw new Error(
        'No participant was selected. Please select one and try again'
      )
    const participants = await findAllParticipants(event_id)
    res.render('print', {
      user: {
        ...user,
        is_owner: user?.user_type === 'OWNER',
        is_admin: user?.user_type === 'MANAGER',
      },
      participants: participants.filter(({ user_id }) => ids.includes(user_id)),
      referer_link: req.headers.referer?.split('?')[0],
    })
  } catch (error) {
    res.render('errors/500', {
      referer_link: req.headers.referer?.split('?')[0],
      message: error,
      user: {
        ...user,
        is_owner: user?.user_type === 'OWNER',
        is_admin: user?.user_type === 'MANAGER',
      },
    })
  }
}
