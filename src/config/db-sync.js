// Importi ng module
const fs = require('fs')
const uuid = require('uuid')
const mysql = require('mysql')
const dotenv = require('dotenv')
const bcrypt = require('bcrypt')
dotenv.config()

const dbConfig = {
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  database: process.env.DB_NAME,

  user: process.env.USER_NAME,
  password: process.env.PASSWORD,
}

let connection

function getConnection() {
  connection = mysql.createConnection(dbConfig)

  connection.connect(function (err) {
    // The server is either down
    if (err) {
      // or restarting (takes a while sometimes).
      console.log('error when connecting to db:', err)
      setTimeout(getConnection, 2000) // We introduce a delay before attempting to reconnect,
    } // to avoid a hot loop, and to allow our node script to
  }) // process asynchronous requests in the meantime.
  // If you're also serving http, display a 503 error.
  connection.on('error', function (err) {
    if (err.code === 'PROTOCOL_CONNECTION_LOST') {
      // Connection to the MySQL server is usually
      getConnection() // lost due to either server restart, or a
    } else {
      console.log('db error', err)
      // connnection idle timeout (the wait_timeout
      throw err // server variable configures this)
    }
  })
}

getConnection()
exports.executeQuery = (query, callback) => {
  try {
    if (typeof callback !== 'function')
      throw new Error('Second argument type most be function')
    connection.query(query, callback)
  } catch (error) {
    console.log('Qerying error: ', error)
  }
}
console.log(`
  -- --------------------------------------------------------
  -- Server Reinitialisation
  -- --------------------------------------------------------
  `)
const sqlString = fs.readFileSync('./youth_space_db.sql').toString()
const sqlQueries = sqlString.split(';')
sqlQueries.pop()
connection.query(
  `
  INSERT INTO
    user(user_id, password, telephone, email, user_type)
  VALUES
    ('${process.env.ADMIN_ID}', '${bcrypt.hashSync(
    process.env.ADMIN_LOGIN,
    10
  )}', '677373540', 'youth-space@gmail.com', 'MANAGER')`,
  (err, result) => {
    if (err) console.log(`Error executing the Setup Query - ${err}`)
    else console.log(result)
  }
)
connection.query(
  `
  INSERT INTO
    user(user_id, password, telephone, email, user_type)
  VALUES
    ('${uuid.v4()}', '${bcrypt.hashSync(
    process.env.OWNER_LOGIN,
    10
  )}', '673016895', 'kuidjamarco@gmail.com', 'OWNER')`,
  (err, result) => {
    if (err) console.log(`Error executing the Setup Query - ${err}`)
    else {
      console.log(result)
      console.log(`
        -- --------------------------------------------------------
        -- All task finished with success
        -- --------------------------------------------------------
        `)
    }
  }
)
sqlQueries.forEach((sqlQuery) => {
  connection.query(sqlQuery, (err, result) => {
    if (err) console.log(`Error executing the Setup Query - ${err}`)
    else console.log(sqlQuery)
  })
})
