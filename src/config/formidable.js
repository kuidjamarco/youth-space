const formidable = require('formidable')
exports.catchFile = (req) => {
  const form = formidable({
    multiples: false,
    keepExtensions: true,
    uploadDir: './public/images',
  })
  return new Promise((resolve, reject) => {
    form.parse(req, (err, fields, files) => {
      if (err) reject(err.message)
      const { newFilename } = files.file
      const img_fields = {
        '/user': { image_ref: newFilename },
        '/admin': { event_flyer: newFilename },
      }
      resolve({ ...img_fields[req.baseUrl], ...fields })
    })
  })
}
