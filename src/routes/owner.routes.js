const express = require('express')
const { getOwnerBoard, getWebStats } = require('../controllers/owner.controllers')

const router = express.Router()

router.get('/', getOwnerBoard)
router.get('/stats', getWebStats)

module.exports = router