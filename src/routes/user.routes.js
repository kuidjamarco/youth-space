const express = require('express')
const {
  registerUser,
  addVisitor,
  loginUser,
  updateLogin,
  updateUser,
  renderUser,
  registerWithAccount,
  updateImage,
} = require('../controllers/user.controllers')
const router = express.Router()


router.post('/log-in', loginUser)

router.post('/visitor', addVisitor)

router.post('/register', registerUser)

router.use(['/', '/update', '/registration'], (req, res, next) => {
  try {
    if (!req.session.user) res.redirect('/?error=Please login and try again')
    else next()
  } catch (error) {
    console.log(error)
  }
})
router.get('/', renderUser)
router.post('/update/log-in', updateLogin)
router.post('/update/image', updateImage)
router.post('/update/user', updateUser)

router.get('/registration/:event_id', registerWithAccount)

router.get('/logout', (req, res) => {
  req.session.destroy()
  res.redirect('/')
})

module.exports = router
