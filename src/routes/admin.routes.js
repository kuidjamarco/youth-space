const express = require('express')
const {
  renderAdminPage,
  renderPrinter,
} = require('../controllers/admin.controllers')
const {
  getServiceStats,
  addStatistics,
} = require('../controllers/service.controller')
const {
  createEvent,
  deleteEvent,
  updateEvent,
  updateEventImage,
} = require('../controllers/event.controllers')

const router = express.Router()

router.get('/', renderAdminPage)
router.get('/print', renderPrinter)

router.get('/stats', getServiceStats)
router.post('/stats/new', addStatistics)

router.post('/event/new', createEvent)
router.get('/event/:event_id/delete', deleteEvent)
router.post('/event/:event_id/edit', updateEvent)
router.post('/event/:event_id/edit-image', updateEventImage)

module.exports = router
