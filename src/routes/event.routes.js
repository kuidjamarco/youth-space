const {
  getAllEvents,
  getAllParticipants,
  renderRegistrationForm,
  getOneEvent,
} = require('../controllers/event.controllers')

const express = require('express')
const router = express.Router()

router.get('/all', getAllEvents)
router.get('/:event_id', getOneEvent)

router.get('/:event_id/participants', getAllParticipants)

router.get('/:event_id/register', renderRegistrationForm)

module.exports = router
