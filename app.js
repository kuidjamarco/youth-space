const uuid = require('uuid')
const { renderHome } = require('./src/controllers/user.controllers')

//dev dependency
const morgan = require('morgan')

//load config
const dotenv = require('dotenv')
dotenv.config()

//syncronize database
require('./src/config/db-sync')

//express import
const express = require('express')
const session = require('express-session')
const { engine: exphbs } = require('express-handlebars')

//production memory strore
const MemoryStore = require('memorystore')(session)

const app = express()

const sess = {
  secret: process.env.SECRET,
  cookie: { maxAge: 24 * 60 * 60 * 1000 },
}
if (process.env.NODE_ENV === 'production') {
  app.set('trust proxy', 1) // trust first proxy
  sess.cookie.secure = true // serve secure cookies
}

app.use(
  session({
    ...sess,
    resave: false,
    genid: uuid.v4,
    saveUninitialized: false,
    store: new MemoryStore({
      checkPeriod: 24 * 60 * 60 * 1000,
    }),
  })
)

// Body parser
app.use(express.urlencoded({ extended: false }))
app.use(express.json())

//client code acces
app.use('/static', express.static('./public'))
if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'))
}

//Handlebars
app.engine(
  '.hbs',
  exphbs({
    defaultLayout: 'main',
    extname: '.hbs',
  })
)
app.set('view engine', '.hbs')
app.set('views', './src/views')

//Primary routes
app.get('/', renderHome)
app.get('/about', (req, res) => {
  const user = req.session.user
  res.render('about', {
    user: {
      ...user,
      is_owner: user?.user_type === 'OWNER',
      is_admin: user?.user_type === 'MANAGER',
    },
  })
})
app.get('/privacy', (req, res) => {
  const user = req.session.user
  res.render('privacy', {
    user: {
      ...user,
      is_owner: user?.user_type === 'OWNER',
      is_admin: user?.user_type === 'MANAGER',
    },
  })
})

//Secondary routes
app.use('/user', require('./src/routes/user.routes'))
app.use('/event', require('./src/routes/event.routes'))
app.use('/assembly', require('./src/routes/assembly.routes'))

//Protected routes
app.use(
  '/admin',
  (req, res, next) => {
    const user = req.session.user
    if (!user || user.user_type !== 'MANAGER') {
      res.redirect(
        `${req.headers.referer?.split('?')[0]}?error=User with email ${
          user?.email
        } does not have the required access for this operation`
      )
    }
    else next()
  },
  require('./src/routes/admin.routes')
)
app.use(
  '/owner',
  (req, res, next) => {
    const user = req.session.user
    if (!user || user.user_type !== 'OWNER') {
      res.render('errors/500', {
        referer_link: req.headers.referer?.split('?')[0],
        message: `User with email ${user?.email} does not have the required access for this operation`,
      })
    }
    else next()
  },
  require('./src/routes/owner.routes')
)

//Not found route
app.use('*', (req, res) => res.render('errors/404'))

const PORT = process.env.PORT || 3000

app.listen(PORT, () => {
  console.log(
    `Youth-space server is running in ${process.env.NODE_ENV} mode on port ${PORT}`
  )
})
