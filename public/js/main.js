const createTableCell = (value, tag) => {
  const colNode = document.createElement('td')
  if (tag === 'img') {
    const image = document.createElement('img')
    image.src = `/static/images/${value}`
    image.style = 'width: 100px; height: 100px'
    colNode.appendChild(image)
  } else {
    const colText = document.createTextNode(value ?? 'N/A')
    colNode.appendChild(colText)
  }
  return colNode
}

const openDialog = (htmlDialogId, hrefs) => {
  const dialogElt = document.querySelector(`#${htmlDialogId}`)
  if (dialogElt) {
    const dialogEltInstance = M.Modal.getInstance(dialogElt)
    const hasSubString = hrefs.reduce(
      (hasSubString, href) => hasSubString || location.href.includes(href),
      false
    )
    if (hasSubString) {
      M.Modal.init(document.querySelector(`#${htmlDialogId}`), {
        dismissible: false,
      })
      dialogEltInstance?.open()
    }
  }
}

const toast = (message, cssClass) => {
  M.toast({
    html: message,
    classes: cssClass,
  })
}

const toastCase = {
  '?error=': 'toast-error',
  '?success=': 'toast-success',
  '?info=': 'toast-info',
}

document.addEventListener('DOMContentLoaded', function () {
  Object.keys(toastCase).forEach((key) => {
    if (location.href.includes(key)) {
      toast(
        `${location.href.split(key)[1].split('%20').join(' ')}`,
        toastCase[key]
      )
    }
  })

  const horizontalCards = document.querySelectorAll('.card')
  if (window.innerWidth < 900) {
    horizontalCards?.forEach((horizontalCard) => {
      horizontalCard.classList.remove('horizontal')
    })
  }
  // materialize initialization
  M.Sidenav.init(document.querySelector('.sidenav'))

  M.FormSelect.init(document.querySelectorAll('select'))

  M.Carousel.init(document.querySelector('.carousel'))

  M.Collapsible.init(document.querySelectorAll('.collapsible'))

  M.Modal.init(document.querySelectorAll('.modal'))

  M.Tabs.init(document.querySelectorAll('.tabs'))

  M.Autocomplete.init(document.querySelector('.autocomplete'))

  M.Datepicker.init(document.querySelectorAll('.datepicker'))

  M.ScrollSpy.init(document.querySelectorAll('.scrollspy'))

  //ckeditor initialization
  if (
    document.querySelector('#description') &&
    document.querySelector('#about')
  ) {
    CKEDITOR.replace('description', {
      plugins: 'wysiwygarea, toolbar, basicstyles, link',
    })
    CKEDITOR.replace('about', {
      plugins: 'wysiwygarea, toolbar, basicstyles, link',
    })
  }
  openDialog('login-dialog', ['/admin', '/owner', '/account'])
  openDialog('success-dialog', ['/event/new'])
  openDialog('event-dialog', ['/admin/event'])

  const deleteLinks = document.querySelectorAll('a.delete')
  const warningDialog = document.querySelector('#warning-dialog')
  const warningDialogInstance = M.Modal.getInstance(warningDialog)
  const confirmDeletionLink = document.querySelector('a.delete-target')
  deleteLinks.forEach((link) => {
    link.addEventListener('click', (e) => {
      e.preventDefault()
      warningDialogInstance?.open()
      confirmDeletionLink.setAttribute('href', link.getAttribute('href'))
    })
  })

  const filesInput = document.querySelectorAll('.file-input')
  const previewDialog = document.querySelector('#preview-image')
  const previewLabel = document.querySelector('#preview-label')
  const previewImageSource = document.querySelector('#preview-image-src')
  if (previewDialog) {
    const previewDialogInstance = M.Modal.getInstance(previewDialog)
    filesInput.forEach((fileInput) => {
      fileInput.addEventListener('change', (e) => {
        previewImageSource.setAttribute(
          'src',
          URL.createObjectURL(e.target.files[0])
        )
        previewLabel.setAttribute('for', fileInput.getAttribute('id'))
        previewDialogInstance.open()
      })
    })
  }

  const htmlEditLinks = document.querySelectorAll('.edit-event')
  const eventDialog = document.querySelector('#edit-event-dialog')
  if (eventDialog) {
    const eventDialogInstance = M.Modal.getInstance(eventDialog)
    htmlEditLinks?.forEach((htmlLink) => {
      htmlLink.addEventListener('click', (e) => {
        const event_id = htmlLink.getAttribute('id')
        eventDialogInstance.open()
        M.updateTextFields()
        fetch(`/event/${event_id}`, {
          method: 'GET',
          headers: { 'Content-Type': 'application/json' },
        })
          .then(async (response) => {
            const { event } = await response.json()
            eventDialog.setAttribute('action', `/admin/event/${event_id}/edit`)
            const eventTitle = document.querySelector('#event-form-title')
            eventTitle.innerHTML = 'update event'
            const eventInputs = document.querySelectorAll('.event-input')
            eventInputs.forEach((input) => {
              const inputId = input.getAttribute('id')
              input.setAttribute(
                'value',
                event[inputId === 'event-name' ? 'name' : inputId]
              )
            })
          })
          .catch((error) => {
            console.log(error.message)
          })
      })
    })
  }

  let toPrintIds = []
  let selectedEventId = ''

  const previewButton = document.querySelector('#preview-button')
  previewButton?.addEventListener('click', (e) => {
    location.href = `/admin/print?event_id=${selectedEventId}&ids=${JSON.stringify(
      toPrintIds
    )}`
  })
  if (toPrintIds.length === 0) previewButton?.setAttribute('disabled', 'true')
  else previewButton?.removeAttribute('disabled')

  const eventSelect = document.querySelector('#event-select')
  eventSelect?.addEventListener('change', (e) => {
    const totalDiv = document.querySelector('#total-registrations')
    fetch(`/event/${e.target.value}/participants`, {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
    })
      .then(async (response) => {
        const { participants } = await response.json()
        selectedEventId = e.target.value
        const total = participants.length
        totalDiv.innerHTML = `${total > 10 ? total : `0${total}`} participants`
        const dataDiv = document.querySelector('#registration-data')
        const htmlNodes = participants.map(
          ({
            user_id,
            first_name,
            last_name,
            image_ref,
            profession,
            assembly,
            legal_status,
            telephone,
            pastor_telephone,
            leader_telephone,
            leader_name,
            pastor_name,
            gender,
          }) => {
            const rowNode = document.createElement('tr')
            rowNode.setAttribute('id', user_id)

            rowNode.append(createTableCell(`${first_name} ${last_name}`))
            rowNode.append(createTableCell(telephone))
            rowNode.append(createTableCell(gender))
            rowNode.append(createTableCell(profession))
            rowNode.append(createTableCell(assembly))
            rowNode.append(createTableCell(legal_status))
            rowNode.append(
              createTableCell(
                `${pastor_name}(${pastor_telephone}), ${leader_name}(${leader_telephone})`
              )
            )
            rowNode.append(createTableCell(image_ref, 'img'))

            const colNode = document.createElement('td')
            colNode.style.textAlign = 'center'

            const span = document.createElement('span')

            const input = document.createElement('input')
            input.classList.add('filled-in')
            input.classList.add('check-input')
            input.setAttribute('type', 'checkbox')
            input.setAttribute('id', user_id)
            input.addEventListener('change', (e) => {
              e.preventDefault()
              if (e.target.checked) {
                toPrintIds.push(user_id)
              } else {
                toPrintIds = toPrintIds.filter((id) => id !== user_id)
              }

              if (toPrintIds.length === 0)
                previewButton?.setAttribute('disabled', 'true')
              else previewButton?.removeAttribute('disabled')
            })

            const label = document.createElement('label')
            label.append(input)
            label.append(span)

            colNode.appendChild(label)
            rowNode.append(colNode)

            return rowNode
          }
        )

        dataDiv.replaceChildren(...htmlNodes)

        const selectAll = document.querySelector('#check-all')
        const checkInputs = document.querySelectorAll('.check-input')
        selectAll.addEventListener('change', (e) => {
          e.preventDefault()
          if (e.target.checked) {
            checkInputs.forEach((input) => {
              this.checked = true
              toPrintIds.push(input.id)
              input.setAttribute('checked', 'checked')
            })
          } else {
            checkInputs.forEach((input) => {
              input.removeAttribute('checked')
              this.checked = false
            })
            toPrintIds = []
          }

          if (toPrintIds.length === 0)
            previewButton?.setAttribute('disabled', 'true')
          else previewButton?.removeAttribute('disabled')
        })
      })
      .catch((error) => {
        console.log(error.message)
      })
  })

  const printButton = document.querySelector('#print-button')
  printButton?.addEventListener('click', (e) => {
    const badges = document.querySelector('#pdf-badges')
    var opt = {
      filename: 'badges.pdf',
      image: { type: 'jpeg', quality: 0.98 },
      html2canvas: { scale: 2 },
      jsPDF: { unit: 'in', format: 'a4', orientation: 'l' },
    }

    // New Promise-based usage:
    html2pdf().set(opt).from(badges).save()
  })
})
