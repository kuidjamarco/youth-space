document.addEventListener('DOMContentLoaded', function () {
  const links = document.querySelectorAll('a')
  const sublink = `/${document.location.href.split('/')[3]}`
  console.log(sublink)
  links.forEach((linkitem) => {
    const parents = document.querySelectorAll('li')
    parents.forEach((parent) => {
      if (
        sublink === linkitem.getAttribute('href') &&
        parent.innerText === linkitem.innerText
      ) {
        parent.classList.add('active')
      }
    })
  })

  const arrowUp = document.querySelector('.btn-floating')
  arrowUp.addEventListener('click', () => {
    const anchor = document.querySelector('#main-anchor')
    anchor.scrollIntoView({
      behavior: 'smooth',
    })
  })

  //search dialog initialization
  let eventRefs = []
  const searchElt = document.querySelector('.autocomplete')
  let searchInstance = M.Autocomplete.getInstance(searchElt)

  fetch('/event/all', {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' },
  })
    .then(async (response) => {
      const { events } = await response.json()
      const data = events.reduce(
        (
          newObject,
          { event_flyer, event_id, name, theme, starting_date, location }
        ) => {
          const option = `${name}, ${theme}, ${starting_date} XAF ${location}`
          newObject[option] = `/static/images/${event_flyer}`
          eventRefs.push({ event_id, option })
          return newObject
        },
        {}
      )
      searchInstance.updateData(data)
    })
    .catch((error) => {
      console.log(error.message)
    })

  if (searchInstance)
    searchInstance.options = {
      ...searchInstance?.options,
      onAutocomplete: (e) => {
        const { event_id } = eventRefs.find(({ option }) => option === e)
        location.href = `/event/${event_id}/register`
      },
    }
})
