const createChart = (htmlNode, dataObject, options) => {
  const { type, color, title, options: more_options } = options
  const data = {
    labels: Object.keys(dataObject),
    datasets: [
      {
        label: title ?? 'My First dataset',
        backgroundColor: color ?? 'rgb(255, 99, 132)',
        borderColor: color ?? 'rgb(255, 99, 132)',
        data: Object.values(dataObject),
      },
    ],
  }

  const config = {
    type: type ?? 'line',
    data: data,
    options: more_options ?? {},
  }
  return new Chart(htmlNode, config)
}

const transform = (dataArray) => {
  let object = {}
  dataArray.forEach(({ date_of_year, value }) => {
    object = { ...object, [date_of_year.split('T')[0]]: value }
  })
  return object
}

document.addEventListener('DOMContentLoaded', function () {
  const visitorNode = document.getElementById('visitor-chart')
  const registrationNode = document.getElementById('registration-chart')
  if (visitorNode && registrationNode) {
    fetch('/owner/stats', {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
    })
      .then(async (response) => {
        const { visitors, registrations } = await response.json()
        const visitorChart = createChart(visitorNode, transform(visitors), {
          title: "Visitor's graph",
        })
        const registrationChart = createChart(
          registrationNode,
          transform(registrations),
          { title: 'Registration graph', color: '#197aff' }
        )
      })
      .catch((error) => {
        console.log(error.message)
      })
  }

  const serviceStatNode = document
    .querySelector('#service-stats-chart')
    .getContext('2d')
  if (serviceStatNode) {
    fetch('/admin/stats', {
      method: 'GET',
      headers: { 'Content-Type': 'application/json' },
    })
      .then(async (response) => {
        const { services } = await response.json()
        const myChart = new Chart(serviceStatNode, {
          type: 'line',
          data: {
            labels: [
              services.map(({ created_at }) =>
                new Date(created_at).toUTCString()
              ),
            ],
            datasets: [
              {
                label: 'Childrens',
                data: [services.map(({ children_number }) => children_number)],
                backgroundColor: 'rgb(255, 99, 132)',
                borderColor: 'rgb(255, 99, 132)',
                borderWidth: 1,
              },
              {
                label: 'Youths',
                data: [services.map(({ youths_number }) => youths_number)],
                backgroundColor: '#197aff',
                borderColor: '#197aff',
                borderWidth: 1,
              },
              {
                label: 'Men',
                data: [services.map(({ men_number }) => men_number)],
                backgroundColor: '#008000',
                borderColor: '#008000',
                borderWidth: 1,
              },
              {
                label: 'Women',
                data: [services.map(({ women_number }) => women_number)],
                backgroundColor: '#fffb00',
                borderColor: '#fffb00',
                borderWidth: 1,
              },
              {
                label: 'Offerings',
                data: [services.map(({ offerings }) => offerings)],
                backgroundColor: '#00e9dd',
                borderColor: '#00e9dd',
                borderWidth: 1,
              },
              {
                label: 'Tite',
                data: [services.map(({ offerings }) => offerings)],
                backgroundColor: '#030707',
                borderColor: '#030707',
                borderWidth: 1,
              },
            ],
          },
          options: {
            scales: {
              y: {
                beginAtZero: true,
              },
            },
          },
        })
      })
      .catch((error) => {
        console.log(error.message)
      })
  }
})
