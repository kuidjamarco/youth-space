-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3308
-- Generation Time: May 27, 2022 at 01:04 PM
-- Server version: 8.0.18
SET
  SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

SET
  AUTOCOMMIT = 0;

START TRANSACTION;

SET
  time_zone = "+00:00";

--
-- Database: `youth_space_db`
--
-- --------------------------------------------------------
--
-- Table structure for table `zone`
--
-- DROP TABLE IF EXISTS `zone`;

CREATE TABLE `zone` (
  `zone_id` VARCHAR(36) NOT NULL,
  `zone_name` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`zone_id`)
);

-- --------------------------------------------------------
--
-- Table structure for table `assembly`
--
-- DROP TABLE IF EXISTS `assembly`;

CREATE TABLE IF NOT EXISTS `assembly` (
  `assembly_id` varchar(36) NOT NULL,
  `name` varchar(100) NOT NULL,
  `pastor_name` varchar(60) NOT NULL,
  `pastor_telephone` int(10) NOT NULL,
  `leader_name` varchar(60) NOT NULL,
  `leader_telephone` int(10) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `zone_id` varchar(36) NOT NULL,
  PRIMARY KEY (`assembly_id`),
  CONSTRAINT `fk_assembly_zone_id` FOREIGN KEY (`zone_id`) REFERENCES `zone`(`zone_id`)
);

-- --------------------------------------------------------
--
-- Table structure for table `user`
--
-- DROP TABLE IF EXISTS `user`;

CREATE TABLE IF NOT EXISTS `user` (
  `user_id` varchar(36) NOT NULL,
  `email` varchar(60) DEFAULT NULL UNIQUE,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `profession` varchar(100) NOT NULL,
  `telephone` int(10) NOT NULL UNIQUE,
  `legal_status` ENUM('MARRIED', 'SINGLE') NULL DEFAULT NULL,
  `gender` ENUM('Male', 'Female') NOT NULL,
  `password` varchar(65) DEFAULT NULL,
  `user_type` ENUM('USER', 'MANAGER', 'OWNER') NOT NULL DEFAULT 'USER',
  `image_ref` varchar(255) NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `assembly_id` varchar(36) NOT NULL,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_user_assembly_id` FOREIGN KEY (`assembly_id`) REFERENCES `assembly`(`assembly_id`)
);

-- --------------------------------------------------------
--
-- Table structure for table `event`
--
-- DROP TABLE IF EXISTS `event`;

CREATE TABLE IF NOT EXISTS `event` (
  `event_id` varchar(36) NOT NULL,
  `name` varchar(60) NOT NULL,
  `starting_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `ending_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `closing_date` datetime DEFAULT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'This is the last day for any user to apply',
  `registration_fee` int(11) DEFAULT NULL,
  `theme` varchar(254) NOT NULL,
  `event_flyer` varchar(255) NULL,
  `location` varchar(254) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(36) NOT NULL,
  PRIMARY KEY (`event_id`),
  CONSTRAINT `fk_event_created_by` FOREIGN KEY(`created_by`) REFERENCES `user`(`user_id`)
);

-- --------------------------------------------------------
--
-- Table structure for table `registration`
--
-- DROP TABLE IF EXISTS `registration`;

CREATE TABLE IF NOT EXISTS `registration` (
  `registration_id` varchar(36) NOT NULL,
  `user_id` varchar(36) NOT NULL,
  `event_id` varchar(36) NOT NULL,
  `registered_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`registration_id`),
  CONSTRAINT `fk_registration_user_id` FOREIGN KEY (`user_id`) REFERENCES `user`(`user_id`),
  CONSTRAINT `fk_registration_event_id` FOREIGN KEY (`event_id`) REFERENCES `event`(`event_id`)
);

-- --------------------------------------------------------
--
-- Table structure for table `service`
--
-- DROP TABLE IF EXISTS `service`;

CREATE TABLE IF NOT EXISTS `service` (
  `service_id` varchar(36) NOT NULL,
  `children_number` int(11) NOT NULL,
  `youths_number` int(11) NOT NULL,
  `women_number` int(11) NOT NULL,
  `men_number` int(11) NOT NULL,
  `event_id` varchar(36) NOT NULL,
  `offerings` int(11) NULL,
  `tite` int(11) NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` varchar(36) NOT NULL,
  PRIMARY KEY (`service_id`),
  CONSTRAINT `fk_service_created_by` FOREIGN KEY(`created_by`) REFERENCES `user`(`user_id`),
  CONSTRAINT `fk_service_event_id` FOREIGN KEY (`event_id`) REFERENCES `event`(`event_id`)
);

-- --------------------------------------------------------
--
-- Table structure for table `visitor`
--
-- DROP TABLE IF EXISTS `visitor`;

CREATE TABLE IF NOT EXISTS `visitor` (
  `visitor_id` varchar(36) NOT NULL,
  `visitor_ip` varchar(30) NOT NULL,
  `email` varchar(36) NOT NULL,
  `visited_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
);

COMMIT;

-- --------------------------------------------------------
-- SETUP DATA SEED
-- --------------------------------------------------------
--
-- District zone seedder
--
INSERT INTO
  `zone`(`zone_id`, `zone_name`)
VALUES
  ('42042046-0f81-4596-bdb1-1f8943e97884', 'NKAMBE');

INSERT INTO
  `zone`(`zone_id`, `zone_name`)
VALUES
  ('c162fa89-b1a4-4f10-a1a4-70f8959d4334', 'AKO');

INSERT INTO
  `zone`(`zone_id`, `zone_name`)
VALUES
  ('eb7a34d4-3d99-4eba-a98c-d96a32006545', 'NWA');

INSERT INTO
  `zone`(`zone_id`, `zone_name`)
VALUES
  ('919ba6eb-9b3f-42f1-bad9-c298157897a5', 'NTUBAW');

-- --------------------------------------------------------
-- Default event
-- 
INSERT INTO
  `event`(`event_id`, `name`, `theme`, `location`,  `event_flyer`, `registration_fee`, `created_by`)
VALUES
  ('78710769-0f0c-4723-b702-123b400955bb', 'District Youth Camp', 'Innovating Youths for Christ', 'Nkambe, Cameroun', 'IMG-20220624-WA0115.jpg', 2500, '2c0d5251-1b57-49e2-a981-e59904b982e0');

-- --------------------------------------------------------
-- Database set up woth success
-- --------------------------------------------------------
;